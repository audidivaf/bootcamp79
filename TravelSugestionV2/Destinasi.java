public class Destinasi{
	String namaDestinasi, speciality;
	int hargaPerjalanan, hargaPenginapan, hargaKuliner, hargaSpeciality;
	int totalPerjalanan, totalPenginapan, totalKuliner, totalSpeciality;
	int hargaTotal;
	int jumlahPenumpang, jumlahHari;
	
	public Destinasi(String namaDestinasi, String speciality, int hargaPerjalanan, int hargaPenginapan, int hargaKuliner, int hargaSpeciality){
		this.namaDestinasi = namaDestinasi;
		this.speciality = speciality;
		this.hargaPerjalanan = hargaPerjalanan;
		this.hargaPenginapan = hargaPenginapan;
		this.hargaKuliner = hargaKuliner;
		this.hargaSpeciality = hargaSpeciality;
	}
	
	public Destinasi(){
		//
	}
	
	public int getPrice(int jumlahPenumpang, int jumlahHari){
		this.jumlahPenumpang = jumlahPenumpang;
		this.jumlahHari = jumlahHari;
		this.totalPerjalanan = hargaPerjalanan*jumlahPenumpang;
		this.totalPenginapan = hargaPenginapan*jumlahPenumpang*jumlahHari;
		this.totalKuliner = hargaKuliner*jumlahPenumpang*jumlahHari;
		this.totalSpeciality = hargaSpeciality*jumlahPenumpang;
		this.hargaTotal = totalPerjalanan + totalPenginapan + totalKuliner + totalSpeciality;
		return hargaTotal;
	}
	
	public void getSummary(String nama) {
		System.out.println("=========+++++++++++++++++++++++++++++===========");
		System.out.println("Berikut adalah Total Biaya Untuk Paket: "+this.namaDestinasi);
		System.out.println();
		System.out.println("Atas Nama                       : "+nama);
		System.out.println("Banyak Orang                    : "+this.jumlahPenumpang);
		System.out.println("Lama Hari                       : "+this.jumlahHari+"\n");
		System.out.println("Transportasi(per-Orang)		:Rp."+this.totalPerjalanan);
		System.out.println("Hotel(Per-Orang,Per-Hari)	:Rp."+this.totalPenginapan);
		System.out.println("Kuliner(Per-Orang,Per-Hari)	:Rp."+this.totalKuliner);		
		System.out.println(this.speciality+"(Per-Orang)\t	:Rp."+this.totalSpeciality);
		System.out.println("------------------------------------------------+");
		System.out.println("Total Biaya Liburan         \t:Rp."+this.hargaTotal);
		
	}
	
}