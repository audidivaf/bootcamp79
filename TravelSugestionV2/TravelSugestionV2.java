import java.util.Scanner;

public class TravelSugestionV2{
	public static void main(String[] args){
		String nama;
		Calculate calculate = new Calculate();
		boolean isExit = false, isBack = false;
		
		Scanner input = new Scanner(System.in);
		int[] price = new int[4];
		
		tampilanAwal();
		do{
			System.out.println("Nama : ");
			nama = input.nextLine();
			String[] arrInputUser = inputUser(input, calculate);
			do{
				isBack = false;
				getCalculate(nama, input, arrInputUser, calculate);
				isBack = backSummary(input, isBack);
			}while(isBack == false);
			exitMenu(input, isExit);			
		}while(isExit == false);
	}
	
	public static String[] inputUser(Scanner input, Calculate calculate){
		int jumlahPenumpang, jumlahHari, budget;
			
		System.out.println("Berapa orang yang akan berlibur : ");
		jumlahPenumpang = input.nextInt();
			
		System.out.println("Berapa lama anda berlibur : ");
		jumlahHari = input.nextInt();
			
		System.out.println("Berapa budget yang dimiliki : ");
		budget = input.nextInt();
		System.out.println();
		
		String[] arrInputUser = calculate.getResult(jumlahPenumpang, jumlahHari, budget);
		return arrInputUser;
	}
	
	public static void getCalculate(String nama, Scanner input, String [] arrInputUser, Calculate calculate){
		if(arrInputUser.length == 0){
			System.out.println("MAAF ANDA TIDAK MEMASUKKAN DATA APAPUN !");
		}else{
			recomendation(nama, input, arrInputUser, calculate);
		}
	}
	
	public static void recomendation(String nama, Scanner input, String[] arrInputUser, Calculate calculate){
		for(int i = 0; i < arrInputUser.length; i++){
		System.out.println("[" + (i+1) + "] " + arrInputUser[i]);
		}
		System.out.println("[0] Keluar");
		System.out.print("Silahkan Pilih Destinasi : ");
		int recomendChooice = input.nextInt();
		switch(recomendChooice){
			case 0: 
				System.exit(0);
			default: 
				if(recomendChooice <= arrInputUser.length){
				checkOut(nama, recomendChooice, arrInputUser, calculate);
			}
		}
	}
	
	public static void checkOut(String nama, int recomendChooice, String[] arrInputUser, Calculate calculate){
		calculate.printBills(arrInputUser[recomendChooice-1], nama);
		System.out.println();
		System.out.println("=========+++++++++++++++++++++++++++++=========== ");
		
		
	}

	public static void tampilanAwal(){
		System.out.println("+===================================+");
		System.out.println("|     Selamat Datang di Aplikasi    |");
		System.out.println("|        Travel Sugestion 79        |");
		System.out.println("|                                   |");
		System.out.println("|Aplikasi ini dapat merekomendasikan|");
		System.out.println("|anda ke destinasi liburan sesuai   |");
		System.out.println("|dengan budget yang anda miliki     |");
		System.out.println("|                                   |");
		System.out.println("|Silahkan isi data sebagai berikut! |");
		System.out.println();
	}
	
	public static boolean backSummary(Scanner input, boolean isBack){
		String userChoose;
		System.out.println("      Apakah Anda Akan Memilih Paket Ini? (Y/N)   ");
		userChoose = input.next();
		if(userChoose.equalsIgnoreCase("Y")){
			isBack = true;
		}
		return isBack;
	}
	
	public static void exitMenu(Scanner input, boolean isExit){
		int userInput;
		System.out.println("|[1] Untuk Memesan Tiket Libutan Lain|");
		System.out.println("|[0] Keluar");
		System.out.println("+====================================+");
		userInput = input.nextInt();
		switch(userInput){
			case 1: 
				isExit = true;
				break;
			case 0: 
				System.exit(0);
				break;
		}
	}

}
