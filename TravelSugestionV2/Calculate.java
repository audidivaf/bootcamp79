import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class Calculate{
	Destinasi lombok = new Destinasi("Lombok", "Snorkling", 2170000, 125000, 75000, 250000);
	Destinasi bangkok = new Destinasi("Bangkok", "Shopping", 3780000, 155000, 55000, 300000);
	Destinasi singapura = new Destinasi("Singapura", "Disney Land", 1200000, 170000, 85000, 360000);
	Destinasi tokyo = new Destinasi("Tokyo", "Ski", 4760000, 170000, 105000, 325000);
	
	public Calculate(){
		//
	}
	
	public String[] getResult(int jumlahPenumpang, int jumlahHari, int budget){
		HashMap<String,Integer> destinationPrice = new HashMap<>();
		
		destinationPrice.put("Lombok",(budget - lombok.getPrice(jumlahPenumpang,jumlahHari)));
		destinationPrice.put("Bangkok",(budget - bangkok.getPrice(jumlahPenumpang,jumlahHari)));
		destinationPrice.put("Singapura",(budget - singapura.getPrice(jumlahPenumpang,jumlahHari)));
		destinationPrice.put("Tokyo",(budget - tokyo.getPrice(jumlahPenumpang,jumlahHari)));
		
		String[] arrStrRekomendasi = hitungRekomendasi(destinationPrice);
		return arrStrRekomendasi;
	}
	
	public String[] hitungRekomendasi(HashMap<String, Integer> destinationPrice) {
		int cnt = 0;
		String[] arrStrRekomendasi = new String[0];
		for(Map.Entry<String,Integer> x: destinationPrice.entrySet()) {
			if(x.getValue()>0) {
				arrStrRekomendasi = Arrays.copyOf(arrStrRekomendasi,arrStrRekomendasi.length+1);
				arrStrRekomendasi[arrStrRekomendasi.length-1]=x.getKey();
				cnt=cnt+1;
			}
		}
		return arrStrRekomendasi;
	}
	
	public void printBills(String strPilihanDestinasi, String nama) {
		switch(strPilihanDestinasi) {
			case "Lombok"	: lombok.getSummary(nama); break;
			case "Bangkok"	: bangkok.getSummary(nama); break;
			case "Singapura": singapura.getSummary(nama); break;
			case "Tokyo"	: tokyo.getSummary(nama); break;
		}
	}
}