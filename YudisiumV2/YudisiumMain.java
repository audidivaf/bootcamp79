import java.util.Scanner;

public class YudisiumMain{	
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		Yudisium yudisium = new Yudisium();
		boolean isExit = false;
		
		tampilanHeader();
		do{
			isExit = false;
			yudisium.run();
			printoutListYudisium(yudisium.getArrMahasiswa());
			isExit = exitMenu(input, isExit);
		}while(isExit == false);
	}
	
	public static void tampilanHeader(){
		System.out.println("=============================================");
		System.out.println();
		System.out.println("          Selamat Datang di Aplikasi         ");
		System.out.println("             PERHITUNGAN YUDISIUM            ");
		System.out.println();
		System.out.println("---------------------------------------------");
		System.out.println();
		System.out.println("   Aplikasi ini dibuat untuk menghitung IPK   ");
		System.out.println("          dan Status YUDISIUM Anda.           ");
		System.out.println("    Silahkan isi beberapa data dibawah ini.   ");
		System.out.println();
		System.out.println("==============================================");
	}
	
	public static void printoutListYudisium(Mahasiswa[] arrMahasiswa) {
		System.out.println("=============================================");
		System.out.println("");
		System.out.println("    Terima Kasih Telah Menggunakan Aplikasi  ");
		System.out.println("            SIMULASI YUDISIUM");
		System.out.println("");
		System.out.println("Berikut merupakan hasil dari yudisium berdasarkan");
		System.out.println("Nilai IPK:");
		System.out.println("");
		printHasilYudisiumMahasiswa(arrMahasiswa);
		System.out.println("==============================================");
		System.out.println("");
	}
	
	public static void printHasilYudisiumMahasiswa(Mahasiswa[] arrMahasiswa) {
		for (int i = 0; i < arrMahasiswa.length; i++){
			System.out.println((i+1)+". "+ arrMahasiswa[i].getNamaMahasiswa()+" - "+
								(String.format("%.2f",arrMahasiswa[i].getIPK())+" - "+ arrMahasiswa[i].getYudisium()));
		}
	}
	
	public static boolean exitMenu(Scanner input, boolean isExit){
		String chooce;
		System.out.println("\nApakah ingin mengulang input ?(Y/N)");
		chooce = input.nextLine();
		if(chooce.equalsIgnoreCase("N")){
			isExit = true;
		}
		return isExit;
	}
}