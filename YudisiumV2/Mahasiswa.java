import java.util.Scanner;

public class Mahasiswa{
	Scanner input;
	private String namaMahasiswa, hasilYudisium;
	private int jumlahMatkul, totalSks, totalNilaiAkhir;
	private double Ipk;
	//array
	private String[] arrMatkul;
	private int[] arrSks;
	private String[] arrNilaiMatkul;
	
	//gettersetter
	public String getNamaMahasiswa(){
		return this.namaMahasiswa;
	}
	public String getYudisium(){
		return this.hasilYudisium;
	}
	public double getIPK(){
		return this.Ipk;
	}
	
	public Mahasiswa(){
		this.input = new Scanner(System.in);
		totalSks = 0;
		totalNilaiAkhir = 0;
	}
	
	public void run(){
		inputData();
		calculate();
	}
	
	public void inputData(){
		System.out.println("\nSilahkan Masukkan Data.");
		System.out.print("Nama\t: ");
		this.namaMahasiswa = input.nextLine();
		this.jumlahMatkul = inputJumlahMatkul(input);
		
		this.arrMatkul = new String[this.jumlahMatkul];
		this.arrSks = new int[this.jumlahMatkul];
		this.arrNilaiMatkul = new String [this.jumlahMatkul];
		
		for(int i = 0; i < this.jumlahMatkul; i++){
			System.out.print("\n\tMata Kuliah\t\t\t\t: ");
			this.arrMatkul[i] = input.nextLine();
			this.arrSks[i] = inputSks(input, this.arrMatkul[i]);
			this.arrNilaiMatkul[i] = inputNilai(input, this.arrMatkul[i]);
		}
	}
	
	public static int inputJumlahMatkul(Scanner input){
		int number = 0;
		System.out.print("\n\tMasukkan jumlah mata kuliah\t\t: ");
		number = inputUserInteger(input);
		while(number < 2 || number > 12){
			System.out.print("\n\tjumlah mata kuliah antara 2 - 12\t: ");
			number = inputUserInteger(input);
		}
		return number;
	}
	public static int inputSks(Scanner input, String namaMatkul){
		int number;
			
		System.out.print("\n\tJumlah SKS mata kuliah " + namaMatkul + "\t: ");
		number = inputUserInteger(input);
			
		while(number < 2 || number > 6){
			System.out.print("\n\tJumlah SKS antara 2 - 6\t: ");
			number = inputUserInteger(input);
		}
		return number;
	}
	
	public static String inputNilai(Scanner input, String namaMatkul){
		String nilaiMatkul;
		
		System.out.print("\n\tNilai mata kuliah " + namaMatkul + " (A,B,C,D,E): ");
		nilaiMatkul = input.next();
		input.nextLine();
		
		while(!(nilaiMatkul.equalsIgnoreCase("A") || nilaiMatkul.equalsIgnoreCase("B") || nilaiMatkul.equalsIgnoreCase("C") || nilaiMatkul.equalsIgnoreCase("D") || nilaiMatkul.equalsIgnoreCase("E"))){
			System.out.print("\n\tMasukkan nilai yang benar!\t: ");
			nilaiMatkul = input.next();
			input.nextLine();
		}
		return nilaiMatkul;
	}
	
	public static int inputUserInteger(Scanner input){
		int choose = -1;
		do{
			try{
				choose = input.nextInt();
			}catch(Exception e){
				System.out.println("\n\tTidak bisa menginput selain angka");
			}
			input.nextLine();
		}while(choose == -1);
		return choose;
	}
	
	public void calculate(){
		this.totalSks = 0;
		this.totalNilaiAkhir = 0;
		int totalNilai = 0;

		for(int i = 0; i < this.jumlahMatkul; i++){
			switch(this.arrNilaiMatkul[i].toUpperCase()){
				case "A": totalNilai = (4 * arrSks[i]);
					break;
				case "B": totalNilai = (3 * arrSks[i]);
					break;
				case "C": totalNilai = (2 * arrSks[i]);
					break;
				case "D": totalNilai = (1 * arrSks[i]);
					break;
				case "E": totalNilai = (0 * arrSks[i]);
					break;
			}
			totalSks += arrSks[i];
			this.totalNilaiAkhir += totalNilai;
		}
		this.Ipk = Double.valueOf(this.totalNilaiAkhir)/Double.valueOf(totalSks);
		hasilYudisium();
	}
	
	public void hasilYudisium(){
		if (this.Ipk >= 3.5){
			this.hasilYudisium = "Cum Laude";
		}else if(this.Ipk >= 2.75){
			this.hasilYudisium = "Sangat Memuaskan";
		}else if(this.Ipk >= 2){
			this.hasilYudisium = "Memuaskan";
		}else{
			this.hasilYudisium = "Pending";
		}
		for(int i = 0; i < this.jumlahMatkul; i++){
			if(this.arrNilaiMatkul[i].equals("E")){
			this.hasilYudisium = "Pending";
			}
		}
	}
}