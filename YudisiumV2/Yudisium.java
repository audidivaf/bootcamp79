import java.util.Scanner;

public class Yudisium{
	Scanner input;
	//object array Mahasiswa
	private Mahasiswa[] arrMahasiswa;
	int jumlahMahasiswa;
	
	public Yudisium(){
		this.input = new Scanner(System.in);
	}
	
	public Mahasiswa[] getArrMahasiswa(){
		return this.arrMahasiswa;
	}
	
	public void run(){
		this.inputData();
		this.listData();
	}
	
	public void inputData(){
		System.out.print("\nSilahkan masukkan jumlah mahasiswa\t: ");
		this.jumlahMahasiswa = inputUserInteger(input);
		this.arrMahasiswa = new Mahasiswa[this.jumlahMahasiswa];
		for(int i = 0; i < this.jumlahMahasiswa; i++){
			this.arrMahasiswa[i] = new Mahasiswa();
			this.arrMahasiswa[i].run();
		}
	}
	
	public static int inputUserInteger(Scanner input){
		int choose = -1;
		do{
			try{
				choose = input.nextInt();
			}catch(Exception e){
				System.out.println("\nTidak bisa menginput selain angka atau 0");
			}
			input.nextLine();
		}while(choose == -1);
		return choose;
	}
	
	public void listData(){
		Mahasiswa mahasiswa = new Mahasiswa();
		for(int x = 0; x<this.arrMahasiswa.length; x++){ 
			for(int y = 0; y< this.arrMahasiswa.length-1; y++){
				if(arrMahasiswa[y].getIPK()<arrMahasiswa[y+1].getIPK()){
					mahasiswa=this.arrMahasiswa[y+1];
					this.arrMahasiswa[y+1]=this.arrMahasiswa[y];
					this.arrMahasiswa[y]=mahasiswa;  
				}
			}
		}
	}
		
}