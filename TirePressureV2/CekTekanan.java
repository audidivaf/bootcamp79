public class CekTekanan{
	boolean isConditionOk = true;
	
	//Contructor
	public CekTekanan(){
		//TODO
	}
	
	public void cekTekanan(int tekananBan, String position){
		if((tekananBan > 42 || tekananBan < 32) && position.equals("FRONT")){
			System.out.println("Tekanan ban diluar batas! Optimal pada tekanan 32 psi - 42 psi");
			this.isConditionOk = false;
		} else if ((tekananBan > 45 || tekananBan < 35) && position.equals("REAR")){
			System.out.println("Tekanan ban diluar batas! Optimal pada tekanan 35 psi - 45 psi");
			this.isConditionOk = false;
		}
	}
	
	public void cekTekanan2(int tekananBan2, String position){
		if((tekananBan2 > 44 || tekananBan2 < 34) && position.equals("FRONT")){
			System.out.println("Tekanan ban diluar batas! Optimal pada tekanan 34 psi - 44 psi");
			this.isConditionOk = false;
		} else if ((tekananBan2 > 47 || tekananBan2 < 37) && position.equals("REAR")){
			System.out.println("Tekanan ban diluar batas! Optimal pada tekanan 37 psi - 47 psi");
			this.isConditionOk = false;
		}
	}
	
	public void cekStability(int selisihTekanan, String position){ 
		if(selisihTekanan > 3 && position.equals("FRONT")){
			System.out.println("Range Terlalu jauh! Optimal selisih kedua ban kurang dari 3");
			this.isConditionOk = false;
		} else if(selisihTekanan > 5 && position.equals("REAR")){
			System.out.println("Range Terlalu jauh! Optimal selisih kedua ban kurang dari 5");
			this.isConditionOk = false;
		}
	}
}