import java.util.Scanner;

public class MainTirePressure{
	public static void main(String[] args){
		Scanner input = new Scanner(System.in);
		CekTekanan calculate = new CekTekanan();
		boolean isExit = false;
		
		header();
		mainMenu(input, calculate);
		System.out.println("Terimakasih Sudah Menggunakan Aplikasi Kami");
	}
	
	public static void header(){
		System.out.println("|===================+++++++++++===================|");
		System.out.println("|Selamat Datang di Aplikasi Tire Pressure  Checker|");
		System.out.println("|                                                 |");
		System.out.println("|Aplikasi ini bertujuan untuk mengukur tekanan ban|");
		System.out.println("|Silahkan ikut instruksi dibawah ini              |");
		System.out.println("|                                                 |");
	}
	
	public static void mainMenu(Scanner input, CekTekanan calculate){
		boolean isExit = false;
		do{
			calculate.isConditionOk = true;
			viewMainMenu();
			switch(menuChooice(input)){
				case 1: 
					inputBanDepan(input, calculate);
					inputBanBelakang(input, calculate);
					printOut(calculate);
					break;
				case 2: 
					inputBanDepan(input, calculate);
					inputBanBelakang(input, calculate);
					inputBanBelakang2(input, calculate);
					printOut(calculate);
					break;
				case 3: 
					inputBanDepan(input, calculate);
					inputBanBelakang(input, calculate);
					inputBanDepan2(input, calculate);
					inputBanBelakang2(input, calculate);
					printOut(calculate);
					break;
				case 0 : isExit = true;
					break;
			}
		}while(isExit == false);
	}
	
	public static int menuChooice(Scanner input){
		int choose = -1;
		do{
			try{
				System.out.println("Kode : ");
				choose = input.nextInt();
			}catch(Exception e){
				System.out.println("Anda tidak memasukkan angka");
			}
			input.nextLine();
		}while(choose == -1 || choose > 3);
		return choose;
	}
	
	public static int inputUser(Scanner input){
		int choose = -1;
		do{
			try{
				choose = input.nextInt();
			}catch(Exception e){
				System.out.println("Anda tidak memasukkan angka");
			}
			input.nextLine();
		}while(choose == -1);
		return choose;
	}
	
	public static void viewMainMenu(){
		System.out.println("|===================+++++++++++===================|");
		System.out.println("|Silahkan pilih jumlah roda kendaraan anda :      |");
		System.out.println("|                                                 |");
		System.out.println("| Kode | Jumlah Roda                              |");
		System.out.println("|-------------------+                             |");
		System.out.println("|  [1] | 4 Roda [A] +                             |");
		System.out.println("|  [2] | 6 Roda [A] +                             |");
		System.out.println("|  [3] | 8 Roda [A] +                             |");
		System.out.println("|  [0] | Keluar     +                             |");
		System.out.println("|===================+++++++++++===================|");
	}
	
	public static void inputBanDepan(Scanner input, CekTekanan calculate){
		int pressFR, pressFL;
		System.out.print("\nSilahkan Masukkan Tekanan Ban Depan Kanan : ");
		pressFR = inputUser(input);
		calculate.cekTekanan(pressFR, "FRONT");
		System.out.print("\nSilahkan Masukkan Tekanan Ban Depan Kiri : ");
		pressFL = inputUser(input);
		calculate.cekTekanan(pressFL, "FRONT");
		calculate.cekStability(Math.abs(pressFR - pressFL), "FRONT");
	}
	
	public static void inputBanDepan2(Scanner input, CekTekanan calculate){
		int pressFR2, pressFL2;
		System.out.print("\nSilahkan Masukkan Tekanan Ban Depan Kanan ke-2 : ");
		pressFR2 = inputUser(input);
		calculate.cekTekanan2(pressFR2, "FRONT");
		System.out.print("\nSilahkan Masukkan Tekanan Ban Depan Kiri ke-2 : ");
		pressFL2 = inputUser(input);
		calculate.cekTekanan2(pressFL2, "FRONT");
		calculate.cekStability(Math.abs(pressFR2 - pressFL2), "FRONT");
	}
	
	public static void inputBanBelakang(Scanner input, CekTekanan calculate){
		int pressRR, pressRL;
		System.out.print("\nSilahkan Masukkan Tekanan Ban Belakang Kanan : ");
		pressRR = inputUser(input);
		calculate.cekTekanan(pressRR, "REAR");
		System.out.print("\nSilahkan Masukkan Tekanan Ban Belakang Kiri: ");
		pressRL = inputUser(input);
		calculate.cekTekanan(pressRL, "REAR");
		calculate.cekStability(Math.abs(pressRR - pressRL), "REAR");
	}
	
	public static void inputBanBelakang2(Scanner input, CekTekanan calculate){
		int pressRR2, pressRL2;
		System.out.print("\nSilahkan Masukkan Tekanan Ban Belakang Kanan ke-2 : ");
		pressRR2 = inputUser(input);
		calculate.cekTekanan2(pressRR2, "REAR");
		System.out.print("\nSilahkan Masukkan Tekanan Ban Belakang Kiri ke-2 : ");
		pressRL2 = inputUser(input);
		calculate.cekTekanan2(pressRR2, "REAR");
		calculate.cekStability(Math.abs(pressRR2 - pressRL2), "REAR");
	}
	
	public static void printOut(CekTekanan calculate){
		System.out.println("***************************************************");
		System.out.print("* Kondisi Tekanan Ban Anda Dalam Kondisi ");
		System.out.print(calculate.isConditionOk == false ? "Tidak Bagus" : "Optimal");
		System.out.print("  *");
		System.out.println("\n***************************************************");
		System.out.println();
	}
}